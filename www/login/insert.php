<?php
header("Content-Type:text/html; charset=utf-8");
//checking if the script received a post request or not
if($_SERVER['REQUEST_METHOD']=='POST'){

 //Getting post data
 $name = $_POST['name'];
 $username = $_POST['username'];
 $password = $_POST['password'];
 $factory = $_POST['factory'];

 //checking if the received values are blank
 if($name == '' || $username == '' || $password == '' || $factory == ''){
 //giving a message to fill all values if the values are blank
 echo '전부 기입해주세요';
 }else{
 //If the values are not blank
 //Connecting to our database by calling dbConnect script
 require_once('dbConnect.php');

 //Creating an SQL Query to insert into database
 //Here you may need to change the retrofit_users because it is the table I created
 //if you have a different table write your table's name

 //This query is to check whether the username or email is already registered or not

 mysqli_query($con, "set session character_set_connection=utf8;");
 mysqli_query($con, "set session character_set_results=utf8;");
 mysqli_query($con, "set session character_set_client=utf8;");

 $sql = "SELECT * FROM users WHERE username='$username'";

 //If variable check has some value from mysqli fetch array
 //That means username or email already exist

 $check = mysqli_fetch_array(mysqli_query($con,$sql));

 //Checking check has some values or not
 if(isset($check)){
 //If check has some value that means username already exist
 echo '아이디가 이미존재합니다.';
 }else{
 //If username is not already exist
 //Creating insert query

 $encrypted_pass = sha1($password);

 $sql = "INSERT INTO users (name,username,password,factory) VALUES('$name','$username','$encrypted_pass','$factory')";

 if($factory!='선택'){

   //Trying to insert the values to db
   if(mysqli_query($con,$sql)){
   //If inserted successfully
    echo '회원가입 완료';
   }else{
   //In case any error occured
   echo '다시시도하세요.';
   }
  } else{
  echo '해당기업/공장을 선택해주세요.';
  }
 }
 //Closing the database connection
 mysqli_close($con);
 }
}else{
echo '에러발생';
}

